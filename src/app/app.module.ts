import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ReferenceService} from './services/reference.service';
import { ReferencesComponent } from './components/references/references.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ReferenceComponent } from './components/reference/reference.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './services/auth.service';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserService} from './services/user.service';
import {AdminGuardService} from './services/admin-guard.service';
import { SearchToolComponent } from './components/search-tool/search-tool.component';
import {HttpClientModule} from '@angular/common/http';
import { CreateReferenceComponent } from './components/create-reference/create-reference.component';
import {GeoRestService} from './services/geo-rest.service';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage';

@NgModule({
  declarations: [
    AppComponent,
    ReferencesComponent,
    NavbarComponent,
    ReferenceComponent,
    SigninComponent,
    SignupComponent,
    SearchToolComponent,
    CreateReferenceComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AppRoutingModule
  ],
  providers: [ReferenceService, AuthService, AdminGuardService, UserService, GeoRestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
