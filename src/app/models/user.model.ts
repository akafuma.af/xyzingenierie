export class User {
  uid: string;
  role: number;

  constructor(uid: string, role: number) {
    this.uid = uid;
    this.role = role;
  }

}

