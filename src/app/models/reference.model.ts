export class Reference {
  id: number;
  nomMission: string;
  nomClient: string;
  ville: string;
  departement: number;
  anneeDebut: string;
  anneeFin: string;
  montantPresta: number;
  detailsPresta: string;
  image: string;
  domaine: string;

  constructor() {}
}
