import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ReferenceService} from '../../services/reference.service';
import {GeoRestService} from '../../services/geo-rest.service';
import {Filter} from '../../models/filter';


@Component({
  selector: 'app-search-tool',
  templateUrl: './search-tool.component.html',
  styleUrls: ['./search-tool.component.css']
})
export class SearchToolComponent implements OnInit {
  searchForm: FormGroup = this.formBuilder.group({
    nomMission: [''],
    nomClient: [''],
    departement: [''],
    ville: [''],
    domaine: [''],
  });

  departements;
  communes;

  filter: Filter;
  showVille = false;
  show = false;

  domaines: string[] = this.referenceService.getDomaines();

  constructor(private formBuilder: FormBuilder, private referenceService: ReferenceService,
              private geoRestService: GeoRestService) { }

  ngOnInit() {
    this.getDepartements();
  }

  /*
  On récupère la liste des départements français via le service GeoRest
  L'objet reçu est un observable on s'abonne donc.
   */
  getDepartements(): void {
    this.geoRestService.getDepartement().subscribe((data: {}) => this.departements = data);
  }

  onChange(): void {
    const v: number = this.searchForm.get('departement').value;
    this.showVille = true;
    // Note : enregistrer la subscription pour unsub ??
    this.geoRestService.getCommunesFromDep(v).subscribe((data) => this.communes = data);
  }

  onSubmit(): void {
    this.filter = {
      nomMission: this.searchForm.get('nomMission').value,
      nomClient: this.searchForm.get('nomClient').value,
      departement: this.searchForm.get('departement').value,
      ville: this.searchForm.get('ville').value,
      domaine: this.searchForm.get('domaine').value
    }

    this.show = true;
  }

}
