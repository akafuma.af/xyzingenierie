import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import * as firebase from 'firebase';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  role: number;

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.authService.user$.subscribe(user => {
      console.log('navbar role ', user.role);
      this.role = user.role;
    });
  }

  onSignOut() {
    this.authService.signOutUser().then(
      () => {
        this.router.navigate(['/references']);
        // window.location.reload();
      }
    );
  }
}
