import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ReferenceService} from '../../services/reference.service';
import {Reference} from '../../models/reference.model';
import {AuthService} from '../../services/auth.service';
import * as firebase from 'firebase';
import {Filter} from '../../models/filter';

@Component({
  selector: 'app-references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.css']
})
export class ReferencesComponent implements OnInit, OnChanges {

  @Input()
  filter: Filter;

  references: Reference[];
  columnsName: string[];
  role: number;

  constructor(private authService: AuthService,
              private referenceService: ReferenceService) {
  }

  ngOnInit() {
    this.authService.user$.subscribe(user => {
      this.role = user.role;
    });
    if (this.filter) {
      this.referenceService.getReferenceWithFilter(this.filter).subscribe((refs) => this.references = refs);
    } else {
      this.getReferences();
    }
    this.columnsName = this.referenceService.getColumnsName();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.referenceService.getReferenceWithFilter(this.filter).subscribe((refs) => this.references = refs);
  }

  getReferences(): void {
    this.referenceService.getRefs().subscribe((refs) => this.references = refs);
  }
}

