import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';
import {ReferenceService} from '../../services/reference.service';
import {Reference} from '../../models/reference.model';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.css']
})
export class ReferenceComponent implements OnInit, OnDestroy {

  private reference: Reference;
  role: number;
  subscription;

  constructor(  private route: ActivatedRoute, private router: Router,
                private referenceService: ReferenceService,
                private location: Location,
                private authService: AuthService) { }

  ngOnInit() {
    this.getReference();
    this.authService.user$.subscribe(user => {
      this.role = user.role;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getReference(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.subscription = this.referenceService.getReferenceById(id)
      .subscribe(ref => {
        this.reference = ref;
      });
  }

  onClickPDF(): void {
    this.referenceService.generatePDF(this.reference, this.role);
  }

  onClickDelete(): void {
    // alert('Supprimer ?');
    this.referenceService.deleteReference(this.reference);
    this.router.navigate(['/references']);
  }
}
