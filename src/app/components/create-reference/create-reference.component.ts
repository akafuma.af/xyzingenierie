import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ReferenceService} from '../../services/reference.service';
import {GeoRestService} from '../../services/geo-rest.service';
import {Reference} from '../../models/reference.model';
import {ActivatedRoute, ParamMap, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-create-reference',
  templateUrl: './create-reference.component.html',
  styleUrls: ['./create-reference.component.css']
})
export class CreateReferenceComponent implements OnInit {

  createForm: FormGroup = this.formBuilder.group({
    nomMission: ['', [Validators.required]],
    nomClient: ['', [Validators.required]],
    departement: ['', [Validators.required]],
    ville: ['', [Validators.required]],
    anneeDebut: ['', [Validators.required, Validators.min(1937)]],
    anneeFin: ['', [Validators.required, Validators.min(1937)]],
    montant: ['', Validators.required],
    details: ['', [Validators.required, Validators.minLength(10)]],
    image: [''],
    domaines: ['', [Validators.required]]
  });

  currentYear = new Date().getFullYear();
  yearRange = Array.from({length: (this.currentYear + 1 - 1937)}, (v, k) => k + 1937);

  departements;
  communes;
  domaines;

  newId;
  refId;

  showVille = false;

  constructor(private formBuilder: FormBuilder, private referenceService: ReferenceService,
              private geoRestService: GeoRestService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getDepartements();
    this.domaines = this.referenceService.getDomaines();

    // On récupère le paramètre de l'url id
    this.refId = this.route.snapshot.paramMap.get('id');
    // Si on a un paramètre id alors on édite la réference
    if (this.refId) {
      this.referenceService.getReferenceById(+this.refId).subscribe((r: Reference) => {

        this.createForm.setValue({
          nomMission: r.nomMission,
          nomClient: r.nomClient,
          departement: r.departement,
          ville: r.ville,
          anneeDebut: r.anneeDebut,
          anneeFin: r.anneeFin,
          montant: r.montantPresta,
          details: r.detailsPresta,
          image: '',
          domaines: r.domaine
        });

        // Nécessaire pour afficher l'input ville et générer les options de ville
        this.onChange();
      });
    } else {
      this.referenceService.getNextId().subscribe((counter) => this.newId = counter.value);
    }
  }

  getDepartements(): void {
    this.geoRestService.getDepartement().subscribe((data: {}) => this.departements = data);
  }

  /*
  Quand un département est selectionné, on requête les villes du département et
  on affiche le menu déroulant des villes
   */
  onChange(): void {
    const v: number = this.createForm.get('departement').value;
    this.showVille = true;
    // Note : enregistrer la subscription pour unsub ??
    this.geoRestService.getCommunesFromDep(v).subscribe((data) => this.communes = data);
  }

  onSubmit(): void {
    // Note : on ignore l'image pour l'instant, à voir avec firebase
    const createdRef: Reference = {
      id: -1,
      nomMission: this.createForm.get('nomMission').value,
      nomClient: this.createForm.get('nomClient').value,
      departement: this.createForm.get('departement').value,
      ville: this.createForm.get('ville').value,
      anneeDebut: this.createForm.get('anneeDebut').value,
      anneeFin: this.createForm.get('anneeFin').value,
      montantPresta: this.createForm.get('montant').value,
      detailsPresta: this.createForm.get('details').value,
      domaine: this.createForm.get('domaines').value,
      image: ''
    };

    if (this.refId) {
      createdRef.id = this.refId;
      this.referenceService.updateReference(createdRef);
    } else {
      createdRef.id = this.newId;
      this.referenceService.saveReference(createdRef);
    }
    // Ne rien faire pour la promise
    this.router.navigate(['/reference/' + createdRef.id]).then();
  }

}
