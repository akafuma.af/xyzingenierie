import { Component } from '@angular/core';
import {ReferenceService} from './services/reference.service';
import {Reference} from './models/reference.model';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'XYZIngenierie';
  refs: Reference[];

  constructor(private referenceService: ReferenceService) {
  }

}
