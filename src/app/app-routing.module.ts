import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReferencesComponent} from './components/references/references.component';
import {ReferenceComponent} from './components/reference/reference.component';
import {SigninComponent} from './components/signin/signin.component';
import {SignupComponent} from './components/signup/signup.component';
import {AdminGuardService} from './services/admin-guard.service';
import {SearchToolComponent} from './components/search-tool/search-tool.component';
import {CreateReferenceComponent} from './components/create-reference/create-reference.component';
import {ChefGuardService} from './services/chef-guard.service';

const routes: Routes = [
  {path: 'references', component: ReferencesComponent},
  {path: 'search', component: SearchToolComponent},
  {path: 'reference/create', canActivate: [ChefGuardService], component: CreateReferenceComponent},
  {path: 'reference/edit/:id', canActivate: [ChefGuardService], component: CreateReferenceComponent},
  {path: 'reference/edit', redirectTo: 'references'},
  {path: 'reference/:id', component: ReferenceComponent},
  {path: 'auth/signin', component: SigninComponent},
  {path: 'auth/signup', canActivate: [AdminGuardService], component: SignupComponent},
  {path: '', redirectTo: 'references', pathMatch: 'full'},
  {path: '**', redirectTo: 'references'}];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
