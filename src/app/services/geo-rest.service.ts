import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GeoRestService {
  private apiURL = 'https://geo.api.gouv.fr/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private httpClient: HttpClient) { }

  /*
  Requête pour récuperer les départements français
   */
  getDepartement() {
    return this.httpClient.get(this.apiURL + 'departements?fields=code');
  }

  getCommunesFromDep(id: number) {
    return this.httpClient.get(this.apiURL + 'departements/' + id + '/communes?fields=nom');
  }
}
