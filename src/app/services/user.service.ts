import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {User} from '../models/user.model';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private db: AngularFirestore) {
  }

  getUserById(id: string) {
    return this.db.collection('users').doc<User>(id).valueChanges();
    /*
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/users/' + id).once('value').then(
          (data) => {
            console.log('getUserById role ' + data.get('role'));
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
     */
  }
}

