import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {User} from '../models/user.model';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {UserService} from './user.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<User>;
  constructor(private userService: UserService, private fireAuthModule: AngularFireAuth, private db: AngularFirestore) {
    this.user$ = this.fireAuthModule.authState.pipe(switchMap(user => {
      if (user) {
        return this.userService.getUserById(user.uid);
      } else {
        return of(new User('notconnected', -1));
      }
    }));
  }

  /*
   * Crée un nouvel utilisateur avec email et password et enregistre son rôle dans la db
   */
  createNewUser(email: string, password: string, role: string) {
    return new Promise(
      (resolve, reject) => {
        this.fireAuthModule.auth.createUserWithEmailAndPassword(email, password).then(
          (userCredential) => {
            console.log('New user created with', userCredential.user.email, userCredential.user.uid);
            const id = userCredential.user.uid;
            this.db.collection('users').doc(id).set({
              uid: id,
              role: +role
            });
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  /*
   * Méthode pour se connecter, récupère le role de l'utilisateur dans la bd
   */
  signInUser(email: string, password: string) {
    return new Promise(
      (resolve, reject) => {
        this.fireAuthModule.auth.signInWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  signOutUser() {
    return this.fireAuthModule.auth.signOut();
  }
}
