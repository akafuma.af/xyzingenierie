import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChefGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        this.authService.user$.subscribe(user => {
          if (user.role > 0) {
            resolve(true);
          } else {
            this.router.navigate(['/references']);
            resolve(false);
          }
        });
      }
    );
  }
}
