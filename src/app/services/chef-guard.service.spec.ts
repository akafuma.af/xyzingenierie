import { TestBed } from '@angular/core/testing';

import { ChefGuardService } from './chef-guard.service';

describe('ChefGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChefGuardService = TestBed.get(ChefGuardService);
    expect(service).toBeTruthy();
  });
});
