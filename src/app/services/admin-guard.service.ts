import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        this.authService.user$.subscribe(user => {
          if (user.role === 2) {
            resolve(true);
          } else {
            this.router.navigate(['/references']);
            resolve(false);
          }
        });
      }
    );
  }
}
