import {Injectable} from '@angular/core';
import {Reference} from '../models/reference.model';
import {Observable, of} from 'rxjs';
import {REFERENCES} from '../data/mock-references';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {AngularFirestore} from '@angular/fire/firestore';
import {Filter} from '../models/filter';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class ReferenceService {

  private domaines: string[] = ['Bâtiment', 'Industrie', 'Aménagement Urbain', 'Transport', 'Eau',
    'Environnement', 'Energie'];

  constructor(private  db: AngularFirestore) {
  }

  getRefs(): Observable<Reference[]> {
    // this.db.collection('references', ref => ref.orderBy('nomMission').limit(30)).valueChanges();
    return this.db.collection<Reference>('references').valueChanges();
  }

  getDomaines(): string[] {
    return this.domaines;
  }

  getNextId(): Observable<any> {
    return this.db.collection('counter').doc('unique').valueChanges();
  }

  getColumnsName() {
    return ['Id', 'Nom mission', 'Nom client', 'Année début', 'Année fin',
      'Département', 'Ville', 'Montant', 'Domaine'];
  }

  /*
  TODO: Propager l'erreur si on ne trouve pas la ref, les composants souscrivant à cette observable doivent fournir une
  méthode pour rediriger l'utilisateur
   */
  getReferenceById(id: number): Observable<Reference> {
    return this.db.collection('references').doc<Reference>(id.toString()).valueChanges();
  }

  getReferenceWithFilter(filter: Filter): Observable<Reference[]> {
    let query = this.db.collection('references').ref.limit(30);
    if (filter.nomMission != '') {
      query = query.where('nomMission', '==', filter.nomMission);
    }
    if (filter.nomClient != '') {
      query = query.where('nomClient', '==', filter.nomClient);
    }
    if (filter.departement > 0) {
      query = query.where('departement', '==', filter.departement);
    }
    if (filter.ville != '') {
      query = query.where('departement', '==', filter.ville);
    }
    if (filter.domaine != '') {
      query = query.where('domaine', 'array-contains', filter.domaine);
    }

    return this.db.collection<Reference>('references', (ref) => query).valueChanges();
  }

  saveReference(reference: Reference): void {
    this.db.collection('references').doc(reference.id.toString()).set(reference);
    //On met à jour le compteur d'id
    this.db.collection('counter').doc('unique').set({value: reference.id + 1});
  }

  updateReference(reference: Reference): void {
    this.db.collection('references').doc(reference.id.toString()).set(reference);
  }

  deleteReference(reference: Reference): void {
    this.db.collection('references').doc(reference.id.toString()).delete();
  }

  generatePDF(reference: Reference, role: number) {
    let nodeMontant = [];

    /*
    Si l'utilisateur a le privilège de voir le montant, on ajoute le noeud au pdf
     */
    if (role > 0) {
      nodeMontant = [{text: 'Montant', style: 'colright', margin: [0, 25, 0, 0]},
        {text: reference.montantPresta + ' €'}];
    }
    const documentDefinition = {
      pageSize: {
        width: 595.28,
        height: 841.89
      },
      background: () => {
        return {
          canvas: [
            {
              type: 'rect',
              x: 0, y: 0, w: 595.28, h: 130,
              color: '#080530'
            }
          ]
        };
      },
      content: [
        {text: reference.nomMission, style: ['top', 'lg'], margin: [0, 2, 0, 20]},
        {text: 'Client : ' + reference.nomClient, style: ['top', 'md'], margin: [0, 10, 0, 10]},
        {text: reference.ville + ' (' + reference.departement + ')', margin: [0, 25, 0, 15]},
        {
          columns: [
            {text: 'image', width: '50%'},
            {
              stack: [
                {text: 'Dates', style: 'colright'},
                {text: reference.anneeDebut + ' - ' + reference.anneeFin},
                nodeMontant],
              width: '50%'
            },
          ],
          margin: [0, 5]
        },
        {text: 'Missions détaillées', color: '#5bc0de', bold: true, style: ['ml'], margin: [0, 5]},
        {text: reference.detailsPresta}
      ],
      footer: {text: 'XYZ Ingénierie', alignment: 'center', style: 'ml'},
      styles: {
        top: {
          color: 'white'
        },
        lg: {
          fontSize: 20
        },
        ml: {
          fontSize: 16
        },
        md: {
          fontSize: 12
        },
        colright: {
          color: 'orange',
          bold: true
        }
      }
    };

    pdfMake.createPdf(documentDefinition).download('reference-' + reference.id);
  }
}
