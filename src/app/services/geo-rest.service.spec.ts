import { TestBed } from '@angular/core/testing';

import { GeoRestService } from './geo-rest.service';

describe('GeoRestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeoRestService = TestBed.get(GeoRestService);
    expect(service).toBeTruthy();
  });
});
