import {Reference} from '../models/reference.model';

/*
 Mock Database
 */
export const REFERENCES: Reference[] = [
  {id: 0, nomMission: 'Commit0', nomClient: 'Lafarge', anneeDebut: '2000',
    anneeFin: '2003', departement: 13,
    detailsPresta: 'Longue prestation, lorem ipsum dolor sit amet',
    domaine: 'BTP', ville: 'Marseille', montantPresta: 69000, image: 'null'},
  {id: 1, nomMission: 'Commit1', nomClient: 'Lafarge', anneeDebut: '2000',
    anneeFin: '2003', departement: 13,
    detailsPresta: 'Longue prestation, lorem ipsum dolor sit amet',
    domaine: 'BTP', ville: 'Marseille', montantPresta: 69000, image: 'null'},
  {id: 2, nomMission: 'Commit2', nomClient: 'Lafarge', anneeDebut: '2000',
    anneeFin: '2003', departement: 13,
    detailsPresta: 'Longue prestation, lorem ipsum dolor sit amet',
    domaine: 'BTP', ville: 'Marseille', montantPresta: 69000, image: 'null'},
  {id: 3, nomMission: 'Commit3', nomClient: 'Lafarge', anneeDebut: '2000',
    anneeFin: '2003', departement: 13,
    detailsPresta: 'Longue prestation, lorem ipsum dolor sit amet',
    domaine: 'BTP', ville: 'Marseille', montantPresta: 69000, image: 'null'},
  {id: 4, nomMission: 'Commit4', nomClient: 'Lafarge', anneeDebut: '2000',
    anneeFin: '2003', departement: 13,
    detailsPresta: 'Longue prestation, lorem ipsum dolor sit amet',
    domaine: 'BTP', ville: 'Marseille', montantPresta: 69000, image: 'null'},
];
