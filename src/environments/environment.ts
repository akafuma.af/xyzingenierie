// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDf_qZsCOEOn90AAICu4Wd1YW99KjMpsHI',
    authDomain: 'xyzingenierie.firebaseapp.com',
    databaseURL: 'https://xyzingenierie.firebaseio.com',
    projectId: 'xyzingenierie',
    storageBucket: 'xyzingenierie.appspot.com',
    messagingSenderId: '653331252696',
    appId: '1:653331252696:web:55ad5d5aba033489691ead'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
