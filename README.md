# TPExam

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Deploy

Nous avons déployé notre application à l'aide de Firebase. Elle est disponible à cette adresse :
https://xyzingenierie.firebaseapp.com

## Running unit tests

Il n'y a pas de test unitaire

## Running end-to-end tests

Il n'y a pas de test e2e

## Mode opératoire

### Fonctionnalités implémentées
- Affichage des références dans la page d'accueil
- Authentification à l'aide de Firebase
- Gestion des différents rôles utilisateurs
- Base de données persistante sur Firebase
- Création/édition des références
- Création/édition des utilisateurs (en tant qu'admin)
- Fonctionnalité de recherche de références (type filtre)
- Export PDF avec PDFMake des fiches références
- Utilisation de l'API REST geo.api.gouv.fr pour récupérer les départements et villes de France
- CSS avec Bootstrap

### A savoir
Plusieurs comptes avec de différents rôles sont disponibles :
- admin@xyz.com
- chef@xyz.com
- salarie@xyz.com

l'authentification avec les comptes existants se fait avec le mot de passe "azerty"